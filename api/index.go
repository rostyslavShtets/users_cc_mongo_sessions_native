package api
import (
	"net/http"
)

// HomePage shows home page with teplate: login or sign up
func HomePage(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "templates/index.html")
}
