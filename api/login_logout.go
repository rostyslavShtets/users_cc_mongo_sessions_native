package api

import (
	"fmt"
	"github.com/satori/go.uuid"
	"users_cc_mongo_sessions_native/models"
	"users_cc_mongo_sessions_native/utils"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"log"
)

// func init() {
// 	store.Options = &sessions.Options{
// 		Path:     "/",
// 		MaxAge:   86400 * 7,
// 		HttpOnly: true,
// 	}
// }

// CheckIfUserIsAuthenticated checks if user have alredy Authenticated
func CheckIfUserIsAuthenticated(w http.ResponseWriter, r *http.Request) (bool, *models.Session) {
	// get cookie
	c, err := r.Cookie("session")
	if err == nil {
		rs, err := daoDB.FindSessionByUUID(c.Value)
		if err != nil {
			// http.Error(w, http.StatusText(500), 500)
			return false, &rs
		}
		fmt.Println(rs.UserEmail)
		return true, &rs
	} else {
		return false, nil
	}
}

// LoginPage for users login
func LoginPage(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "templates/login.html")
		return
	}

	//get user from put request
	userEmail := r.FormValue("userEmail")
	userPassword := r.FormValue("password")
	
	//get request to database
	userFromDB, err := FindUserByEmailEndPointPOST(userEmail)
	if err != nil {
		log.Println("can’t find user:", err)
		http.Redirect(w, r, "/login", 301)
		return
	}

	//compare hash password
	errCrypt := bcrypt.CompareHashAndPassword(userFromDB.Password, []byte(userPassword))

	if (userEmail == userFromDB.Email && errCrypt == nil) {
		// set the cookie
		sID, _ := uuid.NewV4()
		c := &http.Cookie{
			Name:  "session",
			Value: sID.String(),
		}
		http.SetCookie(w, c)

		// save session to database
		var session models.Session
		session.Id = bson.NewObjectId()
		session.UserSessionUUID = c.Value
		session.UserEmail = userFromDB.Email
		if err := daoDB.InsertSession(&session); err != nil {
			fmt.Println("error with insert session")
			http.Error(w, http.StatusText(500), 500)
			return
		}
		
		utils.Tpl.ExecuteTemplate(w, "index_if_login2.gohtml", userFromDB)
		log.Println("You just have login")
				
	} else {
		log.Println("password or user isn't correct", err)
		http.Error(w, "password or user isn't correct", http.StatusBadRequest)
	}
}

// Logout that can logout and to clear session on client and back side
func Logout(w http.ResponseWriter, r *http.Request) {
	// get cookie
	c, err := r.Cookie("session")
	
	// delete session from database
	if err := daoDB.DeleteSession(c.Value); err != nil {
		http.Error(w, http.StatusText(500), 500)
		fmt.Println("can't delete session", err)
		return
	}


	//reset UUID and sent to client
	if err == nil {
		c = &http.Cookie{
			Name:   "session",
			Value:  "",
			MaxAge: -1,
		}
	// 	c.Value = ""
		http.SetCookie(w, c)
	}
	utils.Tpl.ExecuteTemplate(w, "index_if_login3.gohtml", c)
}

