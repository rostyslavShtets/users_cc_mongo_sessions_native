package api

import (
	"users_cc_mongo_sessions_native/dao"
	"users_cc_mongo_sessions_native/config"
)

var configDB = config.Config{}
var daoDB = dao.DAO{}

//InitDB Parse the configuration file 'config.toml', and establish a connection to DB
func InitDB() {
	configDB.Read()

	daoDB.Server = configDB.Server
	daoDB.Database = configDB.Database
	daoDB.Connect()
}

// func DaoAccess() *dao.DAO {
// 	return &daoDB
// }
