package api

import (
	"net/http"
	"users_cc_mongo_sessions_native/models"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"golang.org/x/crypto/bcrypt"
	// "net/http"
	// "fmt"
	// "log"
	// "encoding/json"
	// "io/ioutil"
)

// SignUpPage gives possibility for user's registration
func SignUpPage(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "templates/signup.html")
		return
	}

	userName := r.FormValue("userName")
	userEmail := r.FormValue("email")
	password := r.FormValue("password")
		
	var user models.User
	user.UserName = userName
	user.Email = userEmail
	// crypt the password
	bs, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	user.Password = bs
	
	fmt.Println(user)
			
	user.Id = bson.NewObjectId()

	if err := daoDB.InsertUser(&user); err != nil {
		fmt.Println("error with insert")
		http.Error(w, http.StatusText(500), 500)
		return
	}

	fmt.Fprintf(w, "Person with ID %v created successfully\n", user.Id)
	// http.Redirect(w, r, "/", 301)
}









// 	var user string

// 	err := db.QueryRow("SELECT username FROM users WHERE username=?", username).Scan(&user)

// 	switch {
// 	case err == sql.ErrNoRows:
// 		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
// 		if err != nil {
// 			http.Error(res, "Server error, unable to create your account.", 500)
// 			return
// 		}

// 		_, err = db.Exec("INSERT INTO users(username, password) VALUES(?, ?)", username, hashedPassword)
// 		if err != nil {
// 			http.Error(res, "Server error, unable to create your account.", 500)
// 			return
// 		}

// 		res.Write([]byte("User created!"))
// 		return
// 	case err != nil:
// 		http.Error(res, "Server error, unable to create your account.", 500)
// 		return
// 	default:
// 		http.Redirect(res, req, "/", 301)
// 	}
// }