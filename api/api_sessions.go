package api

import (
	// "users_cc_mongo_sessions_native/models"
	// "github.com/gorilla/mux"
	// "gopkg.in/mgo.v2/bson"
	"net/http"
	// "fmt"
	"log"
	"encoding/json"
	// "io/ioutil"
)

// AllSessionsEndPoint gets list of session
func AllSessionsEndPoint(w http.ResponseWriter, r *http.Request) {
	users, err := daoDB.FindAllSessions()
	if err != nil {
		log.Fatal(err)
		return
	}
	json.NewEncoder(w).Encode(users)
}

