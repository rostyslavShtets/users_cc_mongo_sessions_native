package api

import (
	"net/http"
	"log"
	"encoding/json"
	"fmt"
	"net/url"
	"users_cc_mongo_sessions_native/models"
)
// GetPeopleFilterObject gets object from URL with parametets for filter
func GetPeopleFilterObject(w http.ResponseWriter, r *http.Request) {
	//create istance 
	filterStruct := models.Filter{}
	//handle filter query
	filterFromURL := r.URL.Query().Get("filter")
	filterJSON, err:= url.QueryUnescape(filterFromURL)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	fmt.Println("values:", filterJSON);
	json.Unmarshal([]byte(filterJSON), &filterStruct)
	fmt.Println("values:", filterStruct.FirstName);
	
	// get map from query
	notice := "Query string is:" + r.Method + r.URL.String()
	log.Println(notice)
	
	//working with name
	if filterStruct.FirstName != "" && filterStruct.Age == 0 {
		// var peopleSlc []models.Person
		// err := db.CollectionPeople().Find(bson.M{"firstname": filterStruct.FirstName}).All(&peopleSlc)
		peopleSlc, err := daoDB.FilterObjectOneName(filterStruct.FirstName)
		if err != nil {
			http.Error(w, http.StatusText(400), 400)
			return
		}
		json.NewEncoder(w).Encode(peopleSlc)
		return
	}
		
	//working with name and age
	if filterStruct.FirstName != "" && filterStruct.Age != 0 {
		fmt.Println("working object with name and age")
		peopleSlc, err := daoDB.FilterObjectNameAndAge(filterStruct.FirstName, filterStruct.Age)
		if err != nil {
			http.Error(w, http.StatusText(400), 400)
			return
		}
		json.NewEncoder(w).Encode(peopleSlc)
		return
	}
		
}