package dao

import (
	"log"
	"users_cc_mongo_sessions_native/models"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"fmt"
)

// DAO gives us possibility to configure out connecting to database
type DAO struct {
	Server   string
	Database string
}

var db *mgo.Database

//collections in mongoDB
const (
	PEOPLE = "people"
	BOOKS = "books"
	USERS = "users"
	SESSIONS = "sessions"
)

// Connect - Establish a connection to database
func (p *DAO) Connect() {
	session, err := mgo.Dial(p.Server)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(p.Database)
	fmt.Println("Database is connected!")

	// indexing collections
	IndexPeople(PEOPLE)
	IndexBooks(BOOKS)
}



// FindAllPeople - Find list of people
func (p *DAO) FindAllPeople() ([]models.Person, error) {
	var people []models.Person
	err := db.C(PEOPLE).Find(bson.M{}).All(&people)
	return people, err
}

// FindPersonById - Find a person by its id
func (p *DAO) FindPersonById(id string) (*models.Person, error) {
	res := models.Person{}
	
	if err :=  db.C(PEOPLE).FindId(bson.ObjectIdHex(id)).One(&res); err != nil {
		return nil, err
	}

	return &res, nil
}

// InsertPerson - Insert a person into database
func (p *DAO) InsertPerson(person *models.Person) error {
	return db.C(PEOPLE).Insert(&person)
}

// UpdatePerson - Delete an existing person
func (p *DAO) UpdatePerson(id string, person *models.Person) error {
	return db.C(PEOPLE).Update(bson.M{"_id": bson.ObjectIdHex(id)}, &person)
}

// DeletePerson - Update an existing person
func (p *DAO) DeletePerson(id string) error {
	return db.C(PEOPLE).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}

// filters
// FilterOneName - filter with one name
func (p *DAO) FilterOneName(name string) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": name}).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

// FilterArrayNames - filter with few names
func (p *DAO) FilterArrayNames(namesArray []string) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": bson.M{"$in": namesArray}}).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

// FilterArrayNamesAge - filter with few names and age
func (p *DAO) FilterArrayNamesAge(namesArray []string, personAge int) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": bson.M{"$in": namesArray}, "age": personAge}).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

// FilterArrayNamesAgeSkipLimit - filter with few names and age and skip and limit
func (p *DAO) FilterArrayNamesAgeSkipLimit(namesArray []string, personAge, peopleSkip, peopleLimit int) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": bson.M{"$in": namesArray}}).Skip(peopleSkip).Limit(peopleLimit).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

func (p *DAO) FilterObjectOneName(name string) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": name}).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

func (p *DAO) FilterObjectNameAndAge(name string, personAge int) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": name, "age": personAge}).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

// dao methods for books
//  FindAllBooks - Find list of books
func (p *DAO) FindAllBooks() ([]models.Book, error) {
	var res []models.Book
	err := db.C(BOOKS).Find(bson.M{}).All(&res)
	return res, err
}

// FindBookById - Find a book by its id
func (p *DAO) FindBookById(id string) (*models.Book, error) {
	res := models.Book{}
	
	if err :=  db.C(BOOKS).FindId(bson.ObjectIdHex(id)).One(&res); err != nil {
		return nil, err
	}

	return &res, nil
}

// InsertBook - Insert a book into database
func (p *DAO) InsertBook(book *models.Book) error {
	return db.C(BOOKS).Insert(&book)
}

// UpdateBook - Update an existing book
func (p *DAO) UpdateBook(id string, book *models.Book) error {
	return db.C(BOOKS).Update(bson.M{"_id": bson.ObjectIdHex(id)}, &book)
}

// DeleteBook - Delete an existing book
func (p *DAO) DeleteBook(id string) error {
	return db.C(BOOKS).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}


// dao methods for users
// InsertUser - Insert a user into database
func (p *DAO) InsertUser(user *models.User) error {
	return db.C(USERS).Insert(&user)
}

// FindAllUsers - Find all users
func (p *DAO) FindAllUsers() ([]models.User, error) {
	var res []models.User
	err := db.C(USERS).Find(bson.M{}).All(&res)
	return res, err
}

// FindUserByEmail - Get user by email
func (p *DAO) FindUserByEmail(email string) (models.User, error) {
	var result models.User
	err := db.C(USERS).Find(bson.M{"email": email}).One(&result)
	return result, err
}

// dao methods for sessions
// InsertSession - Insert a session into database
func (p *DAO) InsertSession(session *models.Session) error {
	return db.C(SESSIONS).Insert(&session)
}

// FindSessionByUUID - Get session by session UUID
func (p *DAO) FindSessionByUUID(uuid string) (models.Session, error) {
	var result models.Session
	err := db.C(SESSIONS).Find(bson.M{"usersessionuuid": uuid}).One(&result)
	return result, err
}

// FindAllSessions - Find all sessions
func (p *DAO) FindAllSessions() ([]models.Session, error) {
	var res []models.Session
	err := db.C(SESSIONS).Find(bson.M{}).All(&res)
	return res, err
}

// DeleteSession - Delete an existing session
func (p *DAO) DeleteSession(uuid string) error {
	return db.C(SESSIONS).Remove(bson.M{"usersessionuuid": uuid})
}