package dao

import (
	"gopkg.in/mgo.v2"
	// "gopkg.in/mgo.v2/bson"
)


// IndexPeople - for Index people
func IndexPeople(people string) {
	indexPeople := mgo.Index{
		Key:        []string{"lastname"},
		Unique:     false,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	errIndexPeople := db.C(people).EnsureIndex(indexPeople)
	if errIndexPeople != nil {
		panic(errIndexPeople)
	}
}

// IndexBooks - for Index books
func IndexBooks(books string) {
	indexBooks := mgo.Index{
		Key:        []string{"author"},
		Unique:     false,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	errIndexBooks := db.C(books).EnsureIndex(indexBooks)
	if errIndexBooks != nil {
		panic(errIndexBooks)
	}
}