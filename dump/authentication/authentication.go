// sessions.go
package authentication

import (
	"fmt"
	"net/http"
	"github.com/gorilla/sessions"
	"users_cc_mongo_filter_dao_login/models"
	"users_cc_mongo_filter_dao_login/api"
	// "net/http"
	// "fmt"
	"log"
	"encoding/json"
	"io/ioutil"
)


var (
	key = []byte("super-secret-key")
	store = sessions.NewCookieStore(key)
)

func init() {
	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   86400 * 7,
		HttpOnly: true,
	}
}

func Secret(w http.ResponseWriter, r *http.Request) {
	fmt.Println("secret")
	session, _ := store.Get(r, "cookie-name")

	// Check if user is authenticated
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "Forbidden", http.StatusForbidden)
		return
	}

	// Print secret message
	fmt.Fprintln(w, "The cake is a lie!")
}

func Login(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	//get user from put request
	body, err := ioutil.ReadAll(r.Body)
	var user models.User
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &user)
	if err != nil {
			log.Println("handlers SaveID error:", err)
			http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
			return
		}
	fmt.Println(user)
	
	//get request to database
	userFromDB, err := api.FindUserByEmailEndPointPOST(user.Email)
	if err != nil {
		log.Println("handlers SaveID error:", err)
		http.Error(w, "can’t find user", http.StatusBadRequest)
		w.Write([]byte("can't find such user"))
		json.NewEncoder(w).Encode("Error with user")
		return
	}
	
	if (user.Email == userFromDB.Email && user.Password == userFromDB.Password) {
		
		// Set user as authenticated
		session.Values["authenticated"] = true
		// session.Save(r, w)
		err := session.Save(r, w)

		if err != nil {
			log.Println(err)
		}
		log.Println("You just have login")
		fmt.Fprintf(w, "You just have login %v. Thanks\n", userFromDB.UserName)
		
	} else {
		log.Println("password or user isn't correct", err)
		http.Error(w, "password or user isn't correct", http.StatusBadRequest)
		w.Write([]byte("password or user isn't correct"))
	}
}

func LoginTest(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	// Authentication goes here
	// ...

	// Set user as authenticated
	session.Values["authenticated"] = true
	session.Save(r, w)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	// Revoke users authentication
	session.Values["authenticated"] = false
	session.Save(r, w)
}


//for post

// func Login(w http.ResponseWriter, r *http.Request) {
// 	session, _ := store.Get(r, "cookie-name")

// 	//get user from post request
// 	body, err := ioutil.ReadAll(r.Body)
// 	var user models.User
// 	if err != nil {
// 		log.Println("handlers SaveIDID error:", err)
// 		http.Error(w, "can’t read body", http.StatusBadRequest)
// 		return
// 	}
// 	err = json.Unmarshal(body, &user)
// 	if err != nil {
// 			log.Println("handlers SaveID error:", err)
// 			http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
// 			return
// 		}
// 	fmt.Println(user)
	
// 	//get request to database
// 	userFromDB, err := api.FindUserByEmailEndPointPOST(user.Email)
// 	if err != nil {
// 		log.Println("handlers SaveID error:", err)
// 		http.Error(w, "can’t find user", http.StatusBadRequest)
// 		w.Write([]byte("can't find such user"))
// 		json.NewEncoder(w).Encode("Error with user")
// 		return
// 	}
	
// 	if (user.Email == userFromDB.Email && user.Password == userFromDB.Password) {
		
// 		// Set user as authenticated
// 		session.Values["authenticated"] = true
// 		session.Save(r, w)
// 		log.Println("You just have login")
// 		fmt.Fprintf(w, "You just have login %v. Thanks\n", userFromDB.UserName)
		
// 	} else {
// 		log.Println("password or user isn't correct", err)
// 		http.Error(w, "password or user isn't correct", http.StatusBadRequest)
// 		w.Write([]byte("password or user isn't correct"))
// 	}
// }