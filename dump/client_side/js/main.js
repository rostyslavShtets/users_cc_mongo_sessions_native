
const PORT = "8081"

const API_FOR_STRING = `http://localhost:${PORT}/filter/people?name=`;
const API_FOR_OBJECT = `http://localhost:${PORT}/filter_object/people?filter=`;
const API_FOR_LOGIN = `http://localhost:${PORT}/login?login=`;
const API_FOR_REGISTRATION = `http://localhost:${PORT}/registration?user=`;


// const API_FOR_STRING = "http://localhost:8081/api/people?name=";
// const API_FOR_OBJECT = "http://localhost:8081/api_object/people?filter=";
// const API_FOR_LOGIN = "http://localhost:8081/api_object/people?filter=";

//object with strings for testing filters like string
let queryString = {
  twoNames: "Tom&name=Sandra&name=John",
  twoNamesAndAge: "Tom&name=Sandra&name=John&age=85",
  twoNamesLimitSkip: "Tom&name=Sandra&name=John&limit=10&skip=1"
}

//object for testing filters when we pass object, then encode to json and then to string
// on server side
let objForFilter = {
  firstName: "Tom",
  lastName: "Mongo",
  age: 49
};


// object for login
let objForLogin = {
  userName: "Neo",
  password: "matrix",
  email: "neo@matrix.com"
}

//to find element in DOM
let buttonTestStringFilter = document.getElementById("filterString");
let buttonTestObjectFilter = document.getElementById("filterObject");
let buttonTestLogin = document.getElementById("login");

//to handle events
buttonTestStringFilter.addEventListener("click", () => sendGetRequest(API_FOR_STRING, queryString.twoNames, false));
buttonTestObjectFilter.addEventListener("click", () => sendGetRequest(API_FOR_OBJECT, objForFilter, true));
buttonTestLogin.addEventListener("click", () => sendGetRequest(API_FOR_LOGIN, objForLogin, true));

// func for making get requests to server
function sendGetRequest(api, urlString, itIsForFilterObject) {
  if (itIsForFilterObject) {
    console.log(urlString);
    let objToString = encodeURIComponent(JSON.stringify(urlString))
    urlString = objToString
    console.log(objToString);
  }
  // variable fo url
  let fullUrl = api + urlString;
  // console.log("url:", fullUrl)
  let userName = document.getElementById("userName").value;
  let userPassword = document.getElementById("userPassword").value
  console.log("33223" + userName);

  fetch(fullUrl)
  .then(function(response) {
    console.log(response.headers.get('Content-Type')); // application/json; charset=utf-8
    console.log(response.status); // 200
    // console.log(response)
    return response.json();
   })
  .then(function(user) {
    console.dir(user); // iliakan
  })
  .catch( alert );
}




document.getElementById('postData').addEventListener('submit', postData);

 function postData(event){
    event.preventDefault();

    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;

    fetch('http://localhost:8081/login', {
        method: 'POST',
        body:JSON.stringify(objForLogin)
    }).then((res) => res.json())
    .then((data) => console.log(data))
    .catch((err)=> console.log(err))
}




// function sendPostRequest(instance) {
//   fetch('http://localhost:8081/login', {
//     method: 'post',
//     body: JSON.stringify(instance)
//   }).then(function(response) {
//     return response.json();
//   }).then(function(data) {
//     ChromeSamples.log('Created Gist:', data.html_url);
//   });
// }



