package middleware

import (
	"net/http"
	"users_cc_mongo_sessions_native/api"
	"users_cc_mongo_sessions_native/utils"
	"fmt"
)

// AuthHomePageMiddleware for home page that to check if user have already login: if yes - give him access to books and people points
func AuthHomePageMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	   	if check, s := api.CheckIfUserIsAuthenticated(w, r); check {
			utils.Tpl.ExecuteTemplate(w, "index_if_login.gohtml", s)
			fmt.Println(check, s)
			return
		}
		next(w, r)
    })
}
