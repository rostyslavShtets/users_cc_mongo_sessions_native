package middleware

import (
	"net/http"
	"users_cc_mongo_sessions_native/api"
	// "fmt"
)

// AuthenticationMiddleware - to check if user have already  login: if yes - you can get access, if not - try to login
func AuthenticationMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	   	if check, _ := api.CheckIfUserIsAuthenticated(w, r); check {
			next(w, r)
		} else {
			w.Write([]byte("<h1>you are not login<h1>"))
			return
		}
    })
}