package main

import (
	"users_cc_mongo_sessions_native/api"
	"users_cc_mongo_sessions_native/utils"
	"users_cc_mongo_sessions_native/middleware"
	"github.com/gorilla/mux"
	"fmt"
	"log"
	"net/http"
)
const port = ":8081"

func main() {
	// additional internal check 
	utils.Hello()
	fmt.Println("new_new")

	// init DB
	api.InitDB()
			
	r := mux.NewRouter()
	// r.Path("/").HandlerFunc(homeHandler)

	r.HandleFunc("/", middleware.AuthHomePageMiddleware(api.HomePage))
	r.HandleFunc("/login", api.LoginPage)
	r.HandleFunc("/logout", api.Logout).Methods("GET")
	r.HandleFunc("/signup", api.SignUpPage)
	
	//request for people
	r.HandleFunc("/people", middleware.AuthenticationMiddleware(api.AllPeopleEndPoint)).Methods("GET")
	r.HandleFunc("/people/{personID}", middleware.AuthenticationMiddleware(api.FindPersonEndPoint)).Methods("GET")
	r.HandleFunc("/filter/people",  middleware.AuthenticationMiddleware(api.GetPeopleFilter)).Methods("GET")
	r.HandleFunc("/filter_object/people", api.GetPeopleFilterObject).Methods("GET")
	r.HandleFunc("/people", middleware.AuthenticationMiddleware(api.CreatePersonEndPoint)).Methods("POST")
	r.HandleFunc("/people/{personID}", middleware.AuthenticationMiddleware(api.UpdatePersonEndPoint)).Methods("PUT")
	r.HandleFunc("/people/{personID}", middleware.AuthenticationMiddleware(api.DeletePersonEndPoint)).Methods("DELETE")
	
	//request for books
	r.HandleFunc("/books", middleware.AuthenticationMiddleware(api.AllBooksEndPoint)).Methods("GET")
	r.HandleFunc("/books/{bookID}", middleware.AuthenticationMiddleware(api.FindBookEndPoint)).Methods("GET")
	r.HandleFunc("/books", middleware.AuthenticationMiddleware(api.CreateBookEndPoint)).Methods("POST")
	r.HandleFunc("/books/{bookID}",  middleware.AuthenticationMiddleware(api.UpdateBookEndPoint)).Methods("DELETE")
	r.HandleFunc("/books/{bookID}",  middleware.AuthenticationMiddleware(api.DeleteBookEndPoint)).Methods("PUT")

	//request for users
	r.HandleFunc("/users", api.AllUsersEndPoint).Methods("GET")
	r.HandleFunc("/sessions", api.AllSessionsEndPoint).Methods("GET")

	fmt.Printf("Serv on port %v....\n", port)
	if err := http.ListenAndServe(port, r); err != nil {
		log.Fatal(err)
	}
}


