package utils
import (
	"html/template"
)

// Tpl - we decclare global variable that to use templates in any place of our app
var Tpl *template.Template

func init() {
	Tpl = template.Must(template.ParseGlob("templates/*"))
}