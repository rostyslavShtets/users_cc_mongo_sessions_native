package models

import "gopkg.in/mgo.v2/bson"

// Person represents stuff of organization - data for model
type Person struct {
	Id bson.ObjectId	`json:"id" bson:"_id,omitempty"`
	UserID string		`json:"userID"`
	FirstName string	`json:"firstName"`
	LastName string		`json:"lastName"`
	Age int				`json:"age"`
	Password string		`json:"password"`
}

// Book represents collection of books in shop - data for model
type Book struct {
	Id bson.ObjectId	`json:"id" bson:"_id,omitempty"`
	ISBN    string   	`json:"isbn"`
    Title   string   	`json:"title"`
    Author string 	 	`json:"author"`
    Price   int   		`json:"price"`
}

// Filter represents paramertes for filter data 
type Filter struct {
	Id bson.ObjectId	`json:"id" bson:"_id,omitempty"`
	UserID string		`json:"userID"`
	FirstName string	`json:"firstName"`
	LastName string		`json:"lastName"`
	Age int				`json:"age"`
	Limit int			`json:"limit"`
	Skip int			`json:"skip"`
}

// User represents structure for user registration in our app
type User struct {
	Id bson.ObjectId	`json:"id" bson:"_id,omitempty"`
	UserName string		`json:"userName"`
	Password []byte		`json:"password"`
	Email string		`json:"email"`
}

// Session represents a correspondence between session and user's email
// we can have a lot of sessions with one email
type Session struct {
	Id bson.ObjectId		`json:"id" bson:"_id,omitempty"`
	UserSessionUUID string	`json:"userSessionUUID"`
	UserEmail string		`json:"userEmail"`
}